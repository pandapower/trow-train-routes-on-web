### Mersul trenurilor - TW
This application simulates the routes between two train stations. The application is web oriented and the representation is based on Google Maps. 

The app has many features:
-it has an login module
-the user may save or delete his preffered routes
-it gives results from the current year(2015-2016), but it has an archive with the stored routes from 2013 to 2014  and 2014 to 2015

Video prezentare: 
https://www.youtube.com/watch?v=n1XWcJ5IBxM&feature=youtu.be