﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace xmlParse
{
    class Program
    {
        static void Main(string[] args)
        {
            string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\for_work\facultate\tw\proiect2\trow-train-routes-on-web\project code\proiect2\trow\trow\App_Data\DataBase\trow.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection connection = new SqlConnection(ConnectionString);
            connection.Open();
            XmlDocument proiect = new XmlDocument();
            proiect.Load(@"D:\for_work\facultate\tw\proiect2\trow-train-routes-on-web\project code\proiect2\mers_tren2014.xml");
            //var pr = from p in proiect.Descendants("Trasa") select (string)p.Element("ElementTrasa").Attribute("DenStaDestinatie");
            //foreach(var x in pr)
            //{
            //    Console.WriteLine(x);
            //}
            int trasa = 0;
            int id_element = 0;
            XmlNodeList projs = proiect.GetElementsByTagName("Trasa");
            SqlCommand insert_comand;

            String insert = "";
            insert = "Truncate table ElementeTrasee";            
            insert_comand = new SqlCommand(insert, connection);
           // insert_comand.ExecuteNonQuery();

            foreach (XmlElement proj in projs)
            {
                
                
                insert="insert into TraseeTrenuri2014(id_traseu, CodStatieInitiala, CodStatieFinala) values("+  (++trasa) + ", '"+ proj.GetAttribute("CodStatieInitiala")+"', '"+proj.GetAttribute("CodStatieFinala")+"')" ;
                // selectam nodurile <title>
                insert_comand = new SqlCommand(insert, connection);
                //insert_comand.ExecuteNonQuery();
                Console.WriteLine(insert);
                XmlNodeList titles = proj.SelectNodes("./ElementTrasa");
                foreach (XmlElement title in titles)
                {
                    TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(  title.GetAttribute("OraP"))  );
                    //Console.WriteLine(time.ToString(@"hh\:mm"));

                    TimeSpan time2 = TimeSpan.FromSeconds(Convert.ToDouble(title.GetAttribute("OraS")));
                    //Console.WriteLine(time2.ToString(@"hh\:mm"));

                    double km = Convert.ToDouble(title.GetAttribute("Km"));
                    km = km / 1000;
                    //Console.WriteLine(km.ToString());

                    insert ="insert into ElementeTrasee2014(id_element, DenumireStatieInitiala, DenumireStatieFinala, id_traseu, CodStatieInitiala, CodStatieFinala, OraPlecare, OraSosire, Distanta) values("+ (++id_element)+", '" + title.GetAttribute("DenStaOrigine") + "', '" +title.GetAttribute("DenStaDestinatie")+"', "+trasa+", '"+title.GetAttribute("CodStaOrigine")+"', '"+title.GetAttribute("CodStaDest")+"', '"+ time.ToString(@"hh\:mm") +"', '"+ time2.ToString(@"hh\:mm")+"', '"+km.ToString()+"')" ;
                    insert_comand = new SqlCommand(insert, connection);
                    insert_comand.ExecuteNonQuery();
                    Console.WriteLine();
                    Console.WriteLine(insert);

                }
                // verificam care e clasa proiectului
                Console.WriteLine();
                Console.WriteLine("-------");
               
            }
            // final de "try"

            connection.Close();

        Console.ReadLine();
        }
    }
}
