﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace trow.Models
{
    public class DataBase
    {
        public static SqlConnection GetConnection()
        {
            string ConnectionString = @"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\tw\proiect2\trow-train-routes-on-web\project code\proiect2\trow\trow\App_Data\DataBase\trow.mdf;Integrated Security = True; Connect Timeout = 30";
            SqlConnection connection = new SqlConnection(ConnectionString);
            return connection;
        }

        public static string Register(string username, string email, string password)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            string select = "select * from Users where Username = @user";
            SqlCommand comand = new SqlCommand(select, conn);
            comand.Parameters.AddWithValue("@user", username);
            SqlDataReader reader = comand.ExecuteReader();
            if(reader.HasRows)
            {
                return "0";
            }
            reader.Close();

            string insert = "insert into Users values(3, @user, @email, @pass, 1)";
            SqlCommand insertComand = new SqlCommand(insert, conn);
            insertComand.Parameters.AddWithValue("@user", username);
            insertComand.Parameters.AddWithValue("@email", email);
            insertComand.Parameters.AddWithValue("@pass", password);
            insertComand.ExecuteNonQuery();

            conn.Close();
            return "1";
        }

        
        public static string Login(string username, string password)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            string select = "select * from Users where Username=@user and Password = @pass";
            SqlCommand comand = new SqlCommand(select, conn);
            comand.Parameters.AddWithValue("@user", username);
            comand.Parameters.AddWithValue("@pass", password);
            SqlDataReader reader = comand.ExecuteReader();
            int ok = 1;
            if (!reader.HasRows)
            {
                ok = 0;
            }
            reader.Close();
            conn.Close();
            if (ok==0)
            {
                return "0";
            }
            
            return "1";
        }
    }
}
