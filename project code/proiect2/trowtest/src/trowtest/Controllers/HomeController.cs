﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;

using System.Text;

namespace trowtest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {

            return View();
        }


        public IActionResult Error()
        {

            return View();
        }
        public IActionResult Result()
        {

            return View();
        }
        [HttpPost]
        public string Login(string username, string password)
        { 
            string result = Models.DataBase.Login(username, password);
            if (result=="0")
            {
                return "0";
            }
            //HttpContext.Session.SetString("name", "nume");
            HttpContext.Session.SetString("loged", "1");
            HttpContext.Session.SetString("nume", username);
            return "1";
          
        }
        [HttpPost]
        public string Logout()
        {
            HttpContext.Session.SetString("loged", "0");
            return "1";
        }
        [HttpPost]
        public string Register(string email, string username, string password)
        {
            string result = Models.DataBase.register(email, username, password);
            return result;
        }

        [HttpPost]
        public string Search(string origin, string destination)
        {
            string list = Models.DataBase.Search(origin, destination);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            return "1";
           
        }

        [HttpPost]
        public string SearchOneWay(string origin, string destination)
        {
            string list = Models.DataBase.SearchOneWay(origin, destination);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            return "1";
        }

        [HttpPost]
        public string SearchVia(string origin, string destination, string via)
        {
            string list = Models.DataBase.SearchViaBothWays(origin, destination, via);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            
            return "1";
        }

        [HttpPost]
        public string SearchViaCompose(string origin, string destination, string via)
        {


            HttpContext.Session.SetString("mode", "compose");
            return "1";
        }

        [HttpPost]
        public string SearchViaOneWay(string origin, string destination, string via)
        {
            string list = Models.DataBase.SearchViaOneWay(origin, destination, via);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            return "1";

        }

        [HttpPost]
        public string SearchAvoid(string origin, string destination, string avoid)
        {
            string list = Models.DataBase.SearchAvoid(origin, destination, avoid);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            return "1";

        }

        [HttpPost]
        public string SearchAvoidOneWay(string origin, string destination, string avoid)
        {
            string list = Models.DataBase.SearchAvoidOneWay(origin, destination, avoid);
            HttpContext.Session.SetString("serialize", list);
            HttpContext.Session.SetString("mode", "notcompose");
            return "1";
        }
        [HttpPost]
        public string SendMail(string email_mail, string subiect_mail, string continut_mail)
        {
            //SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            
            
            //client.EnableSsl = true;
        
            
            //client.Credentials = new System.Net.NetworkCredential("andrei.ciornei1@gmail.com", "PandaForce1");
            //MailMessage mm = new MailMessage(email_mail, "andrei.ciornei@outlook.com", subiect_mail, continut_mail);
            //mm.BodyEncoding = UTF8Encoding.UTF8;
           

            //client.Send(mm);
            return "1";
        }
    }
}
