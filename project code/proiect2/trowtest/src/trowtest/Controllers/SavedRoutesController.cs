﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using System.Data.SqlClient;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace trowtest.Controllers
{
    public class SavedRoutesController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            string username = HttpContext.Session.GetString("nume");
            SqlConnection conn = Models.DataBase.GetConnection();
            conn.Open();

            List<Models.SavedRoutes> saved = new List<Models.SavedRoutes>();

            string select = "select * from TraseeFavorite where username = '"+username+"'";
            SqlCommand command = new SqlCommand(select, conn);
            SqlDataReader reader = command.ExecuteReader();

            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    saved.Add(new Models.SavedRoutes(reader.GetString(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3)));
                }
            }

            ViewBag.saved = saved;

            return View();
        }

        [HttpPost]
        public string Delete(int id_ruta)
        {
            string username = HttpContext.Session.GetString("nume");

            SqlConnection conn = Models.DataBase.GetConnection();
            conn.Open();

            string delete = "delete from TraseeFavorite where username='" + username + "' and id_traseu=" + id_ruta;
            SqlCommand command = new SqlCommand(delete, conn);

            command.ExecuteNonQuery();

            conn.Close();

            return "1";
        }
    }
}
