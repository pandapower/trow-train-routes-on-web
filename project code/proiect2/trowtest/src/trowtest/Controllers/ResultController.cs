﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Http;
using Newtonsoft.Json;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace trowtest.Controllers
{
    public class ResultController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            

                string ser = HttpContext.Session.GetString("serialize");

                List<Models.TraseeFinale> list = JsonConvert.DeserializeObject<List<Models.TraseeFinale>>(ser);

                ViewBag.lista = list;
           

            return View();
        }

        [HttpPost]
        public string Save(string id_ruta, string plecare, string sosire)
        {
            string username = HttpContext.Session.GetString("nume");
            string result = Models.DataBase.SaveRoute(id_ruta, plecare, sosire, username);
            return result;
        }

        [HttpPost]
        public string Details(string id_ruta)
        {
            string result = Models.DataBase.Details(id_ruta);
            
            return result;
        }

        [HttpPost]
        public string Details2015(string id_ruta)
        {
            string result = Models.DataBase.Details2015(id_ruta);
             
            return result;
        }

        [HttpPost]
        public string Details2014(string id_ruta)
        {
            string result = Models.DataBase.Details2014(id_ruta);
             
            return result;
        }
    }
}
