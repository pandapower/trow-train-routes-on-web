﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trowtest.Models
{
    public class Traseu
    {
        public string StatieInitiala;
        public string StatieFinala;
        public string OraPlecare;
        public string OraSosire;
        public string km;
        
        public Traseu(string StatieInitiala, string StatieFinala, string OraPlecare, string OraSosire, string km)
        {
            this.StatieFinala = StatieFinala;
            this.StatieInitiala = StatieInitiala;
            this.OraPlecare = OraPlecare;
            this.OraSosire = OraSosire;
            this.km = km;
        }
    }
}
