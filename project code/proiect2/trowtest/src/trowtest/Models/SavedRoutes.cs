﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trowtest.Models
{
    public class SavedRoutes
    {
        public string username;
        public int id_traseu;
        public string plecare;
        public string sosire;
        public SavedRoutes(string username, int id_traseu, string plecare, string sosire)
        {
            this.username = username;
            this.id_traseu = id_traseu;
            this.plecare = plecare;
            this.sosire = sosire;
        }
    }
}
