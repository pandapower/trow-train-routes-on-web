﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.AspNet.Mvc;
using Newtonsoft.Json;
using System;

namespace trowtest.Models
{
    public class DataBase
    {
        public static SqlConnection GetConnection()
        {
            string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\for_work\facultate\tw\proiect2\trow-train-routes-on-web\project code\proiect2\trow\trow\App_Data\DataBase\trow.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection connection = new SqlConnection(ConnectionString);
            return connection;
        }
        
        public static string Login(string username, string password)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            string select = "select * from Users where Username= @user and Password = @pass";
            SqlCommand comand = new SqlCommand(select, conn);
            comand.Parameters.AddWithValue("@user", username);
            comand.Parameters.AddWithValue("@pass", password);
            SqlDataReader reader = comand.ExecuteReader();
            
            
            if (reader.HasRows)
            {
                conn.Close();
                return username;
                
                //HttpContext.Session.SetString("nume", nume.ToString());
            }

            conn.Close();

           
            return "0";
        }

        public static string register(string email, string username, string password)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            string select = "select * from Users where Username= @user";
            SqlCommand comand = new SqlCommand(select, conn);
            comand.Parameters.AddWithValue("@user", username);
            
            SqlDataReader reader = comand.ExecuteReader();
            int ok = 1;
            if(reader.HasRows)
            {
                ok = 0;
            }
            conn.Close();
            
            conn.Open();

            if (ok==1)
            {

                string insert = "insert into Users(Username, Email, Password, Acces) values(@email, @user, @pass ,1)";
                SqlCommand insert_comand = new SqlCommand(insert, conn);

                insert_comand.Parameters.AddWithValue("@email", email);

                insert_comand.Parameters.AddWithValue("@user", username);

                insert_comand.Parameters.AddWithValue("@pass", password);
                //insert_comand.CommandType = CommandType.Text;
                insert_comand.ExecuteNonQuery();

                conn.Close();
                return "1";
            }


            conn.Close();
            return "0";
        }
        [HttpPost]
        public static string SearchOneWay(string origine, string destinatie)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();
            List<Trasee> code_list2 = new List<Trasee>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '" + origine + "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieFinala like '" + destinatie + "%' and id_traseu in(" + list1[0].ToString() + ",";

            for (int i = 1; i < list1.Count - 1; i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";

            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }


            conn.Close();
            conn.Open();

            for (int i = 0; i<code_list.Count; i++)
            {
                insert1 = "select id_element, DenumireStatieInitiala, DenumireStatieFinala from ElementeTrasee where id_traseu=" + code_list[i].id_traseu;
                int dest=0 , orig=0;
                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if(reader.GetString(1).Contains(origine))
                    {
                        orig = reader.GetInt32(0);
                    }
                    else if(reader.GetString(2).Contains(destinatie))
                    {
                        dest = reader.GetInt32(0);
                    }
                }
                if(orig<dest)
                {
                    code_list2.Add(code_list[i]);

                }

                conn.Close();
                conn.Open();
            }

            //string test = list1[1].ToString();

            conn.Close();
            conn.Open();
            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 = 9999, minim2 = 9999;
            for (int i = 0; i < code_list2.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list2[i].CodInitial + "' and id_traseu = " + code_list2[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list2[i].CodFinal + "' and id_traseu = " + code_list2[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list2[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if (secunde1 < secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;
                
                conn.Close();
                conn.Open();

            }

            return JsonConvert.SerializeObject(final_list); ;


            
        }


        [HttpPost]
        public static string Search(string origine, string destinatie)
        {
            
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '"+ origine+ "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while(reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieFinala like '" + destinatie + "%' and id_traseu in(" + list1[0].ToString() + ",";

            for(int i=1; i<list1.Count-1;i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";

            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            //string test = list1[1].ToString();

            conn.Close();
            conn.Open();

            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 =9999, minim2 = 9999;
            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list[i].CodInitial + "' and id_traseu = " + code_list[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list[i].CodFinal + "' and id_traseu = " + code_list[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if(secunde1<secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0) 
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;
                
               
                



                conn.Close();
                conn.Open();

            }

            return JsonConvert.SerializeObject(final_list); ;
        }

        [HttpPost]
        public static string Details(string id_traseu)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            Dictionary<string, Traseu> details = new Dictionary<string, Traseu>();
           
             

            string select = "select DenumireStatieInitiala, DenumireStatieFinala, OraPlecare, OraSosire, Distanta from ElementeTrasee where id_traseu ="+id_traseu;

            command = new SqlCommand(select, conn);
            reader = command.ExecuteReader();
            int i = 0;
            while(reader.Read())
            {

                details.Add((++i).ToString(), new Traseu(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
            }
            
            conn.Close();
            
            return JsonConvert.SerializeObject(details);
        }

        [HttpPost]
        public static string Details2015(string id_traseu)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            Dictionary<string, Traseu> details = new Dictionary<string, Traseu>();



            string select = "select DenumireStatieInitiala, DenumireStatieFinala, OraPlecare, OraSosire, Distanta from ElementeTrasee where id_traseu =" + id_traseu;

            command = new SqlCommand(select, conn);
            reader = command.ExecuteReader();
            int i = 0;
            while (reader.Read())
            {

                details.Add((++i).ToString(), new Traseu(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
            }

            conn.Close();

            return JsonConvert.SerializeObject(details);
        }

        [HttpPost]
        public static string Details2014(string id_traseu)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            Dictionary<string, Traseu> details = new Dictionary<string, Traseu>();



            string select = "select DenumireStatieInitiala, DenumireStatieFinala, OraPlecare, OraSosire, Distanta from ElementeTrasee where id_traseu =" + id_traseu;

            command = new SqlCommand(select, conn);
            reader = command.ExecuteReader();
            int i = 0;
            while (reader.Read())
            {

                details.Add((++i).ToString(), new Traseu(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
            }

            conn.Close();

            return JsonConvert.SerializeObject(details);
        }


        [HttpPost]
        public static string SearchViaBothWays(string origine, string destinatie, string via)
        {

            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();

            List<Trasee> code_list2 = new List<Trasee>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '" + origine + "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where (DenumireStatieFinala  like '" + destinatie + "%') and id_traseu in(" + list1[0].ToString() + ",";

            for (int i = 1; i < list1.Count - 1; i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";
            
            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            //string test = list1[1].ToString();


            conn.Close();
            conn.Open();

            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select id_element, DenumireStatieInitiala, DenumireStatieFinala from ElementeTrasee where id_traseu=" + code_list[i].id_traseu;
                int ok = 0;
                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetString(1).Contains(via))
                    {
                        ok = 1;
                    }

                }
                if (ok==1)
                {
                    code_list2.Add(code_list[i]);

                }

                conn.Close();
                conn.Open();
            }



            conn.Close();
            conn.Open();

            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 = 9999, minim2 = 9999;
            for (int i = 0; i < code_list2.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list2[i].CodInitial + "' and id_traseu = " + code_list2[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list2[i].CodFinal + "' and id_traseu = " + code_list2[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list2[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if (secunde1 < secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;






                conn.Close();
                conn.Open();

            }
            return JsonConvert.SerializeObject(final_list); 
        }


        public static string SearchViaOneWay(string origine, string destinatie, string via)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();

            List<Trasee> code_list2 = new List<Trasee>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '" + origine + "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where (DenumireStatieFinala  like '" + destinatie + "%') and id_traseu in(" + list1[0].ToString() + ",";

            for (int i = 1; i < list1.Count - 1; i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";

            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            //string test = list1[1].ToString();


            conn.Close();
            conn.Open();

            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select id_element, DenumireStatieInitiala, DenumireStatieFinala from ElementeTrasee where id_traseu=" + code_list[i].id_traseu;
                int ok = 0;
                int orig = 0, dest = 0;
                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetString(1).Contains(via))
                    {
                        ok = 1;
                    }
                    if (reader.GetString(1).Contains(origine))
                    {
                        orig = reader.GetInt32(0);
                    }
                    else if (reader.GetString(2).Contains(destinatie))
                    {
                        dest = reader.GetInt32(0);
                    }

                }
                if (ok == 1)
                {
                    if (orig < dest)
                    {
                        code_list2.Add(code_list[i]);
                    }

                }

                conn.Close();
                conn.Open();
            }



            conn.Close();
            conn.Open();

            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 = 9999, minim2 = 9999;
            for (int i = 0; i < code_list2.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list2[i].CodInitial + "' and id_traseu = " + code_list2[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list2[i].CodFinal + "' and id_traseu = " + code_list2[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list2[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if (secunde1 < secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;






                conn.Close();
                conn.Open();

            }

            return JsonConvert.SerializeObject(final_list);
        }

        public static string SearchAvoid(string origine, string destinatie, string avoid)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();

            List<Trasee> code_list2 = new List<Trasee>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '" + origine + "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where (DenumireStatieFinala  like '" + destinatie + "%') and id_traseu in(" + list1[0].ToString() + ",";

            for (int i = 1; i < list1.Count - 1; i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";

            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            //string test = list1[1].ToString();


            conn.Close();
            conn.Open();

            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select id_element, DenumireStatieInitiala, DenumireStatieFinala from ElementeTrasee where id_traseu=" + code_list[i].id_traseu;
                int ok = 0;
                int orig = 0, dest = 0;
                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetString(1).Contains(avoid))
                    {
                        ok = 1;
                    }
                    

                }
                if (ok != 1)
                {
                    
                        code_list2.Add(code_list[i]);
                    

                }

                conn.Close();
                conn.Open();
            }



            conn.Close();
            conn.Open();

            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 = 9999, minim2 = 9999;
            for (int i = 0; i < code_list2.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list2[i].CodInitial + "' and id_traseu = " + code_list2[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list2[i].CodFinal + "' and id_traseu = " + code_list2[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list2[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if (secunde1 < secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;






                conn.Close();
                conn.Open();

            }

            return JsonConvert.SerializeObject(final_list);
        }


        public static string SearchAvoidOneWay(string origine, string destinatie, string avoid)
        {
            SqlConnection conn = GetConnection();
            conn.Open();
            SqlDataReader reader;
            SqlCommand command;
            List<int> list1 = new List<int>();
            List<int> final_id = new List<int>();
            List<Trasee> code_list = new List<Trasee>();
            List<TraseeFinale> final_list = new List<TraseeFinale>();

            List<Trasee> code_list2 = new List<Trasee>();

            string insert1 = "select distinct id_traseu from ElementeTrasee where DenumireStatieInitiala like '" + origine + "%'";

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                list1.Add(reader.GetInt32(0));
            }

            insert1 = "select distinct id_traseu from ElementeTrasee where (DenumireStatieFinala  like '" + destinatie + "%') and id_traseu in(" + list1[0].ToString() + ",";

            for (int i = 1; i < list1.Count - 1; i++)
            {
                insert1 += list1[i].ToString() + ", ";
            }

            insert1 += list1[list1.Count - 1].ToString() + ")";

            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                final_id.Add(reader.GetInt32(0));
            }

            insert1 = "select * from TraseeTrenuri where id_traseu in(" + final_id[0].ToString() + ",";

            for (int i = 1; i < final_id.Count - 1; i++)
            {
                insert1 += final_id[i].ToString() + ", ";
            }

            insert1 += final_id[final_id.Count - 1].ToString() + ")";


            conn.Close();
            conn.Open();

            command = new SqlCommand(insert1, conn);
            reader = command.ExecuteReader();

            while (reader.Read())
            {
                code_list.Add(new Trasee(reader.GetInt32(0), reader.GetString(1), reader.GetString(2)));
            }

            //string test = list1[1].ToString();


            conn.Close();
            conn.Open();

            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select id_element, DenumireStatieInitiala, DenumireStatieFinala from ElementeTrasee where id_traseu=" + code_list[i].id_traseu;
                int ok = 0;
                int orig = 0, dest = 0;
                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetString(1).Contains(avoid))
                    {
                        ok = 1;
                    }
                    if (reader.GetString(1).Contains(origine))
                    {
                        orig = reader.GetInt32(0);
                    }
                    else if (reader.GetString(2).Contains(destinatie))
                    {
                        dest = reader.GetInt32(0);
                    }


                }
                if (ok != 1)
                {
                    if (orig < dest)
                    {
                        code_list2.Add(code_list[i]);

                    }
                }

                conn.Close();
                conn.Open();
            }



            conn.Close();
            conn.Open();

            int pozitia_minim = 0;
            double secunde1 = 0, secunde2 = 0;
            //TimeSpan minim1 = new TimeSpan();
            //TimeSpan minim2 = new TimeSpan();
            double minim1 = 9999, minim2 = 9999;
            for (int i = 0; i < code_list.Count; i++)
            {
                insert1 = "select DenumireStatieInitiala, OraPlecare from ElementeTrasee where CodStatieInitiala='" + code_list[i].CodInitial + "' and id_traseu = " + code_list[i].id_traseu;
                string denInitiala = "";
                string denFinala = "";
                string orap = "";
                string oras = "";


                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denInitiala = reader.GetString(0);
                    orap = reader.GetString(1);
                }



                conn.Close();
                conn.Open();

                insert1 = "select DenumireStatieFinala, OraSosire from ElementeTrasee where CodStatieFinala='" + code_list[i].CodFinal + "' and id_traseu = " + code_list[i].id_traseu;

                command = new SqlCommand(insert1, conn);
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    denFinala = reader.GetString(0);
                    oras = reader.GetString(1);
                }

                final_list.Add(new TraseeFinale(denInitiala, denFinala, code_list[i].id_traseu, orap, oras));

                secunde1 = TimeSpan.Parse(orap).TotalSeconds;
                secunde2 = TimeSpan.Parse(oras).TotalSeconds;

                double ore = TimeSpan.Parse("23:59").TotalSeconds;

                if (secunde1 < secunde2)
                {
                    minim2 = secunde2 - secunde1;
                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }

                }
                else
                {
                    minim2 = (ore - secunde1) + secunde2;

                    if (i == 0)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }
                    if (minim2 < minim1)
                    {
                        minim1 = minim2;
                        final_list[pozitia_minim].minim = 0;
                        pozitia_minim = i;
                        final_list[pozitia_minim].minim = 1;

                        // TimeSpan time = TimeSpan.FromSeconds();
                        final_list[pozitia_minim].durata = TimeSpan.FromSeconds(minim2).ToString(@"hh\:mm");
                    }


                }
                //minim2 = t2 - t1;






                conn.Close();
                conn.Open();

            }

            return JsonConvert.SerializeObject(final_list);
        }

        public static string SaveRoute(string id_ruta, string plecare, string sosire, string username)
        {
            SqlConnection conn = GetConnection();
            conn.Open();


            string insert = "insert into TraseeFavorite(username, id_traseu, plecare, sosire) values('" + username + "', " + id_ruta + ", '" + plecare + "', '" + sosire + "')";
            SqlCommand command = new SqlCommand(insert, conn);
            command.ExecuteNonQuery();

            return "1";
        }
    }
}
