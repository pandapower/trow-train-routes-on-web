﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trowtest.Models
{
    public class Trasee
    {
        public int id_traseu;
        
        public string CodInitial;
        public string CodFinal;
       

        public Trasee(int id_traseu, string CodInitial, string CodFinal)
        {
            this.CodFinal = CodFinal;
            this.CodInitial = CodInitial;
            this.id_traseu = id_traseu;
        }
    }
}
