﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trowtest.Models
{
    public class TraseeFinale
    {
        public string DenumireOrigine;
        public string DenumireFinal;
        public int id_traseu;
        public string OraP;
        public string OraS;
        public int minim = 0;
        public string durata = "";
        public TraseeFinale(string denumireOrigine, string denumireFinal, int id_traseu, string OraP, string OraS)
        {
            this.DenumireFinal = denumireFinal;
            this.DenumireOrigine = denumireOrigine;
            this.id_traseu = id_traseu;
            this.OraP = OraP;
            this.OraS = OraS;
        }
    }
}
